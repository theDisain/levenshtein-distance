pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven {
            url = uri("https://dl.bintray.com/gradle/gradle-plugins")
        }
    }

    val kotlinVersion = "1.5.31"
    plugins {
        kotlin("kapt") version kotlinVersion apply false
        kotlin("jvm") version kotlinVersion apply false
        kotlin("kapt") version kotlinVersion apply false
        kotlin("plugin.allopen") version kotlinVersion apply false
        kotlin("plugin.spring") version kotlinVersion apply false
        kotlin("plugin.jpa") version kotlinVersion apply false
    }
}

rootProject.name = "sentence-service"

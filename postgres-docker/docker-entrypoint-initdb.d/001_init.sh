set -e

psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='sentence'" | grep -q 1 || psql -c "CREATE USER sentence WITH PASSWORD 'sentence';"

createdb -O sentence sentence -e
createdb -O sentence sentence-test -e
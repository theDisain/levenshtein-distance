import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED
import org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    idea
    id("org.springframework.boot") version "2.5.6"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("com.palantir.docker") version "0.30.0"
    id("com.palantir.docker-compose") version "0.30.0"
    kotlin("jvm") version "1.5.31"
    kotlin("plugin.spring") version "1.5.31"
    kotlin("plugin.jpa") version "1.5.31"
    kotlin("kapt")
}

group = "ee.ainsus"
version = "0.0.2"

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

lateinit var archiveLocalPath: Provider<RegularFile>
lateinit var archiveLocalName: String

repositories {
    mavenCentral()
    google()
    gradlePluginPortal()
}

dependencyManagement {
    imports {
        mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.flywaydb:flyway-core")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springdoc:springdoc-openapi-ui:1.5.2")
    runtimeOnly("org.postgresql:postgresql")
    implementation("com.vladmihalcea:hibernate-types-52:2.10.0")
    implementation("org.springframework.boot:spring-boot-starter-security")


    // Mapstruct
    implementation("org.mapstruct:mapstruct:1.4.1.Final")
    kapt("org.mapstruct:mapstruct-processor:1.4.1.Final")
    // Mapstruct

    // Jackson
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-hppc")
    implementation("com.fasterxml.jackson.module:jackson-module-jaxb-annotations")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-hibernate5")
    implementation("com.fasterxml.jackson.core:jackson-databind")
    // Jackson

    implementation("org.zalando:problem:0.26.0-RC.0")
    implementation("org.zalando:problem-spring-web:0.26.1")

    implementation("javax.servlet:javax.servlet-api")
    implementation("io.springfox:springfox-core:3.0.0")
    implementation("org.hibernate:hibernate-envers")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}


sourceSets {
    create("integrationTest") {
        withConvention(org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet::class) {
            kotlin.srcDir("src/integrationTest/kotlin")
            resources.srcDir("src/integrationTest/resources")
            compileClasspath += sourceSets["main"].output + configurations["testRuntimeClasspath"]
            runtimeClasspath += output + compileClasspath + sourceSets["test"].runtimeClasspath
        }
    }
}

idea {
    module {
        testSourceDirs.add(File("src/integrationTest"))
        testResourceDirs.add(File("src/integrationTest/resources"))
    }
}

tasks {
    withType<JavaCompile> {
        sourceCompatibility = "11"
        targetCompatibility = "11"
    }

    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=enable")
            jvmTarget = "11"
            javaParameters = true
        }
    }
    val integrationTest by creating(Test::class) {
        environment["SPRING_PROFILES_ACTIVE"] = environment["SPRING_PROFILES_ACTIVE"] ?: "env-integration-test"
        description = "Runs the integration tests"
        group = LifecycleBasePlugin.VERIFICATION_GROUP
        testClassesDirs = sourceSets["integrationTest"].output.classesDirs
        classpath = sourceSets["integrationTest"].runtimeClasspath
        testLogging {
            events("FAILED", "SKIPPED")
        }
    }
    create<TestReport>("integrationTestReport") {
        destinationDir = file("$buildDir/reports/tests")
        reportOn(integrationTest)
    }

    withType<Test> {
        useJUnitPlatform()
    }

    build {
        finalizedBy("generateDockerCompose")
    }

    test {
        exclude("**/*IT*")
        testLogging {
            events(FAILED, SKIPPED)
        }
    }

    withType<BootJar> {
        archiveLocalPath = archiveFile
        archiveLocalName = archiveFileName.get()
    }
}

docker {
    dependsOn(tasks.getByName("build"))
    name = "${project.name}:${project.version}"

    setDockerfile(file("Dockerfile"))

    files(archiveLocalPath)

    buildArgs(
        mapOf(
            "JAR_FILE" to archiveLocalName,
        )
    )
}

dockerCompose {
    setTemplate("docker-compose.yml.template")

    setDockerComposeFile("docker-compose.yml")

    templateToken("jarFile", archiveLocalName)
}

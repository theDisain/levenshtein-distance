package ee.ainsus.fatiguetest

import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.support.TransactionTemplate
import javax.persistence.EntityManager


@AutoConfigureMockMvc
@SpringBootTest(classes = [LevenshteinDistanceApp::class])
@Sql(scripts = ["classpath:sql/clean-integration-db.sql"], executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
abstract class IntegrationTest {

    @Autowired
    private lateinit var transactionManager: PlatformTransactionManager

    @Autowired
    lateinit var mvc: MockMvc

    @Autowired
    lateinit var em: EntityManager

    lateinit var transactionTemplate: TransactionTemplate

    @BeforeEach
    fun setup() {
        transactionTemplate = TransactionTemplate(transactionManager)
    }

    fun <T> persist(entity: T): T? = transactionTemplate.execute {
        em.persist(entity)
        entity
    }
}

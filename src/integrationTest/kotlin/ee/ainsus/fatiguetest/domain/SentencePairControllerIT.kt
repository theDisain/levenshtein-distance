package ee.ainsus.fatiguetest.domain

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import ee.ainsus.fatiguetest.IntegrationTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class SentencePairControllerIT : IntegrationTest() {
    private companion object {
        private val mapper = jacksonObjectMapper()
        private const val URL = "/api/sentence"
    }

    @Value("classpath:json/domain/domain_create_input.json")
    private lateinit var domainCreateResource: Resource

    @Value("classpath:json/domain/domain_create_illegal_input.json")
    private lateinit var domainCreateIllegalResource: Resource

    @Value("classpath:json/domain/domain_update_input.json")
    private lateinit var domainUpdateResource: Resource

    @Test
    fun `should create sentence`() {
        val responseJson = mvc.perform(
            post(URL)
                .content(domainCreateResource.inputStream.readBytes())
                .contentType(APPLICATION_JSON)
        ).andExpect(status().isCreated)
            .andReturn().response.contentAsString

        val domain = mapper.readValue<SentencePairDTO>(responseJson)
        assertNotNull(domain.id)
    }

    @Test
    fun `should update domain`() {
        persist(SentencePair("test_domain"))!!
        val updatedString = "TEST_DOMAIN_UPDATED"

        val responseJson = mvc.perform(
            put(URL)
                .content(domainUpdateResource.inputStream.readBytes())
                .contentType(APPLICATION_JSON)
        ).andExpect(status().isOk)
            .andReturn().response.contentAsString

        val domain = mapper.readValue<SentencePairDTO>(responseJson)
        assertNotNull(domain.id)
        assertEquals(updatedString, domain.originalText)
    }

    @Test
    fun `should not create domain when ID added`() {
        mvc.perform(
            post(URL)
                .content(domainCreateIllegalResource.inputStream.readBytes())
                .contentType(APPLICATION_JSON)
        ).andExpect(status().isBadRequest)
    }

    @Test
    fun `should get domain`() {
        val sentencePair = persist(SentencePair("test_domain", "test_domein", 1))!!
        val responseJson = mvc.perform(
            get("$URL/${sentencePair.id}")
                .contentType(APPLICATION_JSON)
        ).andExpect(status().isOk)
            .andReturn().response.contentAsString

        val result = mapper.readValue<SentencePairDTO>(responseJson)

        assertEquals(sentencePair.id, result.id)
        assertEquals(sentencePair.originalText, result.originalText)
    }

    @Test
    fun `should find domain`() {
        val sentencePair = persist(SentencePair("test_domain", "test_domein", 1))!!

        val responseJson = mvc.perform(
            get(URL)
                .contentType(APPLICATION_JSON)
        ).andExpect(status().isOk)
            .andReturn().response.contentAsString

        val response = mapper.readValue<List<SentencePairDTO>>(responseJson)
        assertEquals(1, response.size)
        assertEquals(sentencePair.originalText, response[0].originalText)
    }

    @Test
    fun `should find domain by name`() {
        val domainName = "test_domain"
        val sentencePair = persist(SentencePair(domainName, "test_domein", 1))!!

        persist(SentencePair("OTHER"))

        val responseJson = mvc.perform(
            get("$URL?search=name:$domainName")
                .contentType(APPLICATION_JSON)
        ).andExpect(status().isOk)
            .andReturn().response.contentAsString
        val response = mapper.readValue<List<SentencePairDTO>>(responseJson)

        assertEquals(1, response.size)
        assertEquals(sentencePair.originalText, response[0].originalText)
    }
}

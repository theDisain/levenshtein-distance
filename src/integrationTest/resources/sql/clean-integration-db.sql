DO '
DECLARE
    _schemas_to_clear TEXT[] := ARRAY [
        ''satellite''
        ];
    _table_names_to_ignore TEXT[] := ARRAY [''system'', ''expected_completion_unit_type'', ''schema_version'']; -- any table that does not have an ''id'' column is automatically ignored
    _targets TEXT;
BEGIN
SELECT string_agg(format(''%s.%s'', t.table_schema, t.table_name), '', '') INTO _targets
FROM information_schema.tables t
    INNER JOIN information_schema.columns c
    ON c.table_schema = t.table_schema
        AND c.table_name = t.table_name
WHERE t.table_schema = ANY (_schemas_to_clear)
    AND t.table_type = ''BASE TABLE''
    AND NOT (t.table_name = ANY (_table_names_to_ignore));

EXECUTE (format(''TRUNCATE TABLE %s RESTART IDENTITY CASCADE'', _targets));

END;
';

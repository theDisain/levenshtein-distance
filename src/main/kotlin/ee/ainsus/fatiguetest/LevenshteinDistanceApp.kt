package ee.ainsus.fatiguetest

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.core.env.Environment
import javax.annotation.PostConstruct

@SpringBootApplication
class LevenshteinDistanceApp(private val env: Environment) {

    @PostConstruct
    fun initApplication() {
        if (env.activeProfiles.isEmpty()) {
            log.warn("No Spring profile configured, running with default configuration")
        } else {
            log.info("Running with Spring profile(s) : {}", env.activeProfiles.contentToString())
            val activeProfiles: Collection<String> = env.activeProfiles.toList()
            if (activeProfiles.contains("env-local") &&
                activeProfiles.contains("env-docker")
            ) {
                log.error("local and docker running concurrently! You're doing something worng!")
            }
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(LevenshteinDistanceApp::class.java)

        @JvmStatic
        @SuppressWarnings("SpreadOperator")
        fun main(args: Array<String>) {
            val env = runApplication<LevenshteinDistanceApp>(*args).environment
            logApplicationStartup(env)
        }

        @JvmStatic
        private fun logApplicationStartup(env: Environment) {

            val serverPort = env.getProperty("server.port")
            val serverAddress = env.getProperty("spring.datasource.url")
            val contextPath = env.getProperty("server.servlet.context-path") ?: "/"

            log.info(
                """

                ----------------------------------------------------------
                Application '${env.getProperty("spring.application.name")}' is running! Access URLs:
                Database:   $serverAddress
                Local:      localhost:$serverPort$contextPath
                Profile(s): ${env.activeProfiles.joinToString(",")}
                ----------------------------------------------------------
                """.trimIndent()
            )
        }
    }
}

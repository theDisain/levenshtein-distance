package ee.ainsus.fatiguetest.error

data class ErrorsDTO(
    val errors: MutableList<ErrorDTO> = mutableListOf()
) {
    fun addError(code: String, message: String, location: String? = null) {
        errors.add(ErrorDTO(code, message, location))
    }
}

data class ErrorDTO(
    val code: String,
    val message: String,
    val location: String?,
)
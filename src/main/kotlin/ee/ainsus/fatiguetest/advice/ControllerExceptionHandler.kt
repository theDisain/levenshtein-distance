package ee.ainsus.fatiguetest.advice

import ee.ainsus.fatiguetest.error.BadRequestExceptionMessage
import ee.ainsus.fatiguetest.error.ErrorsDTO
import org.apache.commons.lang3.exception.ExceptionUtils
import org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage
import org.slf4j.LoggerFactory
import org.springframework.dao.NonTransientDataAccessException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.MediaType.APPLICATION_PROBLEM_JSON
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.springframework.web.util.WebUtils
import java.sql.SQLException
import javax.persistence.EntityNotFoundException

@RestControllerAdvice
class ControllerExceptionHandler : ResponseEntityExceptionHandler() {
    private companion object {
        private const val INTEGRITY_CONSTRAINT_VIOLATION_SQL_STATE_START = "23"
        private const val DATA_VALIDATION_EXCEPTION_SQL_STATE = "CDVA0"
        private const val RAISE_EXCEPTION_SQL_STATE = "P0001"
        private val LOG = LoggerFactory.getLogger(ControllerExceptionHandler::class.java)
    }

    @ExceptionHandler(NonTransientDataAccessException::class)
    fun handleNonTransientDataAccessException(
        ex: NonTransientDataAccessException,
        request: WebRequest
    ): ResponseEntity<Any> {
        val mostSpecificCause = ex.mostSpecificCause
        if (mostSpecificCause is SQLException) {
            val sqlState: String? = mostSpecificCause.sqlState
            if (sqlState != null) {
                if (sqlState.startsWith(INTEGRITY_CONSTRAINT_VIOLATION_SQL_STATE_START) ||
                    listOf(DATA_VALIDATION_EXCEPTION_SQL_STATE, RAISE_EXCEPTION_SQL_STATE).contains(sqlState)
                ) {
                    val message = getSqlExceptionMessage(getRootCauseMessage(ex))
                    return ResponseEntity
                        .badRequest()
                        .body(ErrorsDTO().apply {
                            addError(
                                "error.sql.rootCause",
                                message ?: "could not parse error"
                            )
                        })
                }
            }
        }
        return handleExceptionInternal(ex, ex, HttpHeaders(), BAD_REQUEST, request)
    }

    @ExceptionHandler(Exception::class)
    fun handleRootException(e: Exception, request: WebRequest): ResponseEntity<Any> {
        LOG.error("Technical error {}", ExceptionUtils.getStackTrace(e))
        return handleExceptionInternal(e, null, HttpHeaders(), INTERNAL_SERVER_ERROR, request)
    }

    @ExceptionHandler(BadRequestExceptionMessage::class)
    fun handleBadRequestException(e: BadRequestExceptionMessage, request: WebRequest): ResponseEntity<Any> {
        LOG.error("Bad Request {}", ExceptionUtils.getStackTrace(e))
        handleExceptionInternal(e, e.message, HttpHeaders(), BAD_REQUEST, request)
        return ResponseEntity
            .status(BAD_REQUEST)
            .contentType(APPLICATION_PROBLEM_JSON)
            .body(ErrorsDTO().apply {
                addError(e.parameters["message"].toString(), getRootCauseMessage(e), e.parameters["params"].toString())
            })
    }

    @ExceptionHandler(EntityNotFoundException::class)
    fun handleEntityNotFoundException(e: EntityNotFoundException): ResponseEntity<ErrorsDTO> {
        LOG.error("Entity not found {}", ExceptionUtils.getStackTrace(e))
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .contentType(APPLICATION_PROBLEM_JSON)
            .body(ErrorsDTO().apply {
                addError("error.notFound", getRootCauseMessage(e))
            })
    }

    override fun handleExceptionInternal(
        ex: java.lang.Exception,
        body: Any?,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        if (INTERNAL_SERVER_ERROR == status) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST)
        }
        val errors = ErrorsDTO()
        errors.addError("error.rootCause", getRootCauseMessage(ex))
        return ResponseEntity
            .status(status)
            .contentType(APPLICATION_PROBLEM_JSON)
            .headers(headers)
            .body(errors)
    }

    fun getSqlExceptionMessage(message: String): String? {
        val cleanedMessage = removeFromStartIncluding(message, "ERROR: ")
        return removeFromEnd(cleanedMessage, "Where: ")
    }

    private fun removeFromStartIncluding(message: String, endingWith: String): String {
        val index = message.indexOf(endingWith)
        return if (index == -1) message else message.substring(index + endingWith.length)
    }

    private fun removeFromEnd(message: String, startingFrom: String): String? {
        val endIndex = message.indexOf(startingFrom)
        return if (endIndex == -1) message else message.substring(0, endIndex).trim { it <= ' ' }
    }
}

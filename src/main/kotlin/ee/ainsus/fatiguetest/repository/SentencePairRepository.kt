package ee.ainsus.fatiguetest.repository;

import ee.ainsus.fatiguetest.common.CrudRepository
import ee.ainsus.fatiguetest.domain.SentencePair

interface SentencePairRepository : CrudRepository<SentencePair, Long> {
}
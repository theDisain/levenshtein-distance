package ee.ainsus.fatiguetest.common.specification

import org.springframework.data.jpa.domain.Specification

class SpecificationsBuilder<T> {
    private val criteria: MutableList<SearchCriteria> = mutableListOf()
    private val customSpec: MutableList<Specification<T>> = mutableListOf()

    fun with(key: String, operation: String, value: String): SpecificationsBuilder<T> {
        criteria.add(SearchCriteria(key, operation, value))
        return this
    }

    fun addSpecification(spec: Specification<T>): SpecificationsBuilder<T> {
        customSpec.add(spec)
        return this
    }

    fun build(): Specification<T>? {
        if (criteria.size == 0 && customSpec.size == 0) return null
        var result: Specification<T>? = null
        if (criteria.size != 0) {
            val specs: List<Specification<T>> = criteria.map { CrudSpecification(it) }
            result = specs[0]
            for (i in 1 until criteria.size) {
                result = Specification.where(result).and(specs[i])
            }
        }
        if(customSpec.size != 0) {
            for (i in 0 until customSpec.size) {
                result = if(result == null) Specification.where(customSpec[i]) else Specification.where(result).and(customSpec[i])
            }
        }
        return result
    }
}

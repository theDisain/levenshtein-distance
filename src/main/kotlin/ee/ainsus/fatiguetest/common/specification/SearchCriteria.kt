package ee.ainsus.fatiguetest.common.specification

data class SearchCriteria(
    val key: String,
    val operation: String,
    val value: String,
)

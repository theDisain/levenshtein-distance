package ee.ainsus.fatiguetest.common.specification

import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root


class CrudSpecification<T>(private val criteria: SearchCriteria) : Specification<T> {

    override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when {
            criteria.operation.equals(">", true) -> {
                builder.greaterThanOrEqualTo(root.get(criteria.key), criteria.value)
            }
            criteria.operation.equals("<", true) -> {
                builder.lessThanOrEqualTo(root.get(criteria.key), criteria.value)
            }
            criteria.operation.equals(":", true) -> {
                val isValueString = root.get<Any>(criteria.key).javaType == String::class.java
                if (isValueString) builder.like(root.get(criteria.key), "%${criteria.value}%")
                else root.get<String>(criteria.key).`in`(criteria.value.split(","))
            }
            else -> null
        }
    }
}

package ee.ainsus.fatiguetest.common

import ee.ainsus.fatiguetest.domain.IdEntity
import ee.ainsus.fatiguetest.domain.IdEntityDTO
import ee.ainsus.fatiguetest.domain.IdEntityMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.transaction.annotation.Transactional


private const val GENERIC_AUTOWIRE = "SpringJavaInjectionPointsAutowiringInspection"

abstract class CrudService<DTO : IdEntityDTO, E : IdEntity> {

    @Autowired
    @Suppress(GENERIC_AUTOWIRE)
    protected lateinit var mapper: IdEntityMapper<DTO, E>

    @Autowired
    @Suppress(GENERIC_AUTOWIRE)
    private lateinit var repository: CrudRepository<E, Long>

    open fun save(entityDTO: DTO): DTO {
        return mapper.map(repository.save(mapper.map(entityDTO)))
    }

    @Transactional(readOnly = true)
    open fun findAll(pageable: Pageable): Page<DTO> {
        return repository.findAll(pageable).map(mapper::map)
    }

    @Transactional(readOnly = true)
    open fun findAll(spec: Specification<E>?, pageable: Pageable): Page<DTO> {
        return repository.findAll(spec, pageable).map(mapper::map)
    }

    @Transactional(readOnly = true)
    open fun findOne(id: Long): DTO? {
        return repository.findById(id).map(mapper::map).orElse(null)
    }

    open fun delete(id: Long) = repository.deleteById(id)
}

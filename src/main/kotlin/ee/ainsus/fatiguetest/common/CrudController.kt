package ee.ainsus.fatiguetest.common

import ee.ainsus.fatiguetest.common.specification.SpecificationsBuilder
import ee.ainsus.fatiguetest.domain.IdEntity
import ee.ainsus.fatiguetest.domain.IdEntityDTO
import ee.ainsus.fatiguetest.error.BadRequestExceptionMessage
import io.swagger.v3.oas.annotations.Parameter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springdoc.core.converters.models.PageableAsQueryParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.nio.charset.StandardCharsets.UTF_8
import java.util.regex.Pattern
import javax.validation.Valid


private const val GENERIC_AUTOWIRE = "SpringJavaInjectionPointsAutowiringInspection"
private val DTO_REGEX = Regex("DTO$")
private val searchPattern = Pattern.compile("(\\w+)([:<>])([\\w,]+)")

@Validated
abstract class CrudController<DTO : IdEntityDTO, E : IdEntity> {
    companion object {
        val LOG: Logger = LoggerFactory.getLogger(CrudController::class.java)
        private const val FIND_DESCRIPTION = "Syntax example: `?search=id:137;field:value`"
    }

    @Autowired
    @Suppress(GENERIC_AUTOWIRE)
    private lateinit var service: CrudService<DTO, E>

    @PostMapping
    fun create(@Valid @RequestBody entity: DTO): ResponseEntity<DTO> {
        val entityName = getEntityName(entity)
        logCrudEvent(entity, "REST request to save")
        if (entity.id != null) {
            throw BadRequestExceptionMessage(
                "A new $entityName cannot already have an ID", entityName,
                "id_exists"
            )
        }
        val result = service.save(entity)
        val uri = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(result.id).toUri()
        val headers = createCreatedUpdatedEventMessageHeaders(result, "CREATED ENTITY")
        return ResponseEntity.created(uri).headers(headers).body(result)
    }

    @PutMapping
    fun update(@Valid @RequestBody entity: DTO): ResponseEntity<DTO> {
        val entityName = getEntityName(entity)
        logCrudEvent(entity, "REST request to update")
        if (entity.id == null) {
            throw BadRequestExceptionMessage("Invalid id", entityName, "id_null")
        }
        val result = service.save(entity)
        val header = createCreatedUpdatedEventMessageHeaders(result, "UPDATED ENTITY")
        return ResponseEntity.ok().headers(header).body(result)
    }

    @GetMapping
    @PageableAsQueryParam
    fun find(
        @Parameter(hidden = true) pageable: Pageable,
        @Parameter(description = FIND_DESCRIPTION) @RequestParam(required = false) search: String?
    ): Page<DTO> {
        LOG.debug("Initiated findAll event")
        if (search != null) {
            val builder = SpecificationsBuilder<E>()
            val matcher = searchPattern.matcher(search)
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3))
            }

            val spec = builder.build()
            return service.findAll(spec, pageable)
        }
        return service.findAll(pageable)
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): ResponseEntity<DTO?> {
        LOG.debug("REST request to get entity '{0}'", id)
        return ResponseEntity.ok()
            .body(service.findOne(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND))
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): ResponseEntity<Void> {
        LOG.debug("REST request to delete entity '{0}'", id)

        service.delete(id)
        val header = createDeletedEventHeaders(id)
        return ResponseEntity.noContent().headers(header).build()
    }

    private fun createCreatedUpdatedEventMessageHeaders(entity: DTO, message: String): HttpHeaders {
        val entityName = getEntityName(entity)

        return createHeaders(message, entityName, entity.id!!)
    }

    private fun createDeletedEventHeaders(entityId: Long ): HttpHeaders = createHeaders(
        "ENTITY DELETED",
        null,
        entityId
    )


    private fun createHeaders(
        message: String,
        entityName: String?,
        entityId: Long
    ): HttpHeaders {
        val headers = HttpHeaders()
        headers.add("satellite-control", "$message, ${entityName ?: ""}")
        try {
            headers.add("satellite-control-entity-id", URLEncoder.encode(entityId.toString(), UTF_8.toString()))
        } catch (e: UnsupportedEncodingException) {
            //Realistically doesn't happen, there isn't a version of java that doesn't handle UTF-8
        }
        return headers
    }

    private fun logCrudEvent(entity: DTO, message: String) {
        val entityName = getEntityName(entity)

        LOG.debug("$message {0} : {1}", entityName, entity)
    }

    private fun getEntityName(entity: DTO) = entity.javaClass.simpleName.replaceFirst(DTO_REGEX, "")
}

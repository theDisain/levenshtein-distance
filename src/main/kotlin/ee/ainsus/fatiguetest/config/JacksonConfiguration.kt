package ee.ainsus.fatiguetest.config

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.zalando.problem.ProblemModule

@Configuration
class JacksonConfiguration {

    @Bean
    fun hibernate5Module() = Hibernate5Module()

    @Bean
    fun problemModule() = ProblemModule()
}

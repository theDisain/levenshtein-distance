package ee.ainsus.fatiguetest.controller

import ee.ainsus.fatiguetest.common.CrudController
import ee.ainsus.fatiguetest.domain.SentencePair
import ee.ainsus.fatiguetest.domain.SentencePairDTO
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/api/sentence")
@RestController
@CrossOrigin
class SentencePairController : CrudController<SentencePairDTO, SentencePair>()
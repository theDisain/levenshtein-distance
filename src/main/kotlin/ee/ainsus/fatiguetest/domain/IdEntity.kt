package ee.ainsus.fatiguetest.domain

import org.hibernate.Hibernate
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass


@MappedSuperclass
abstract class IdEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        return if (other !is IdEntity || Hibernate.getClass(this) != Hibernate.getClass(other)) {
            false
        } else id != null && other.id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()
}
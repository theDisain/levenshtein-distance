package ee.ainsus.fatiguetest.domain

import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface SentencePairMapper : IdEntityMapper<SentencePairDTO, SentencePair>
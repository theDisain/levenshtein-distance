package ee.ainsus.fatiguetest.domain

abstract class IdEntityDTO(var id: Long? = null)
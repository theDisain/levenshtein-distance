package ee.ainsus.fatiguetest.domain

interface IdEntityMapper<DTO : IdEntityDTO, E : IdEntity> {
    fun map(dto: DTO): E
    fun map(e: E): DTO
}

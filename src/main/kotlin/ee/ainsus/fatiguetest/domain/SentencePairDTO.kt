package ee.ainsus.fatiguetest.domain

import org.springframework.data.annotation.ReadOnlyProperty

data class SentencePairDTO(
    var originalText: String,
    var fixedText: String,
    @ReadOnlyProperty
    var levenshteinScore: Long
) : IdEntityDTO()

package ee.ainsus.fatiguetest.domain

import org.hibernate.Hibernate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Table(name = "sentence_pair", schema = "words")
@Entity
data class SentencePair (

    @Column(name = "original_text", nullable = false)
    var originalText: String? = null,
    @Column(name = "fixed_text", nullable = false)
    var fixedText: String? = null,
@Column(name = "levenshtein_score", nullable = false)
    var levenshteinScore: Long? = null
) : IdEntity() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as SentencePair

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , originalText = $originalText , fixedText = $fixedText , levenshteinScore = $levenshteinScore )"
    }

}
package ee.ainsus.fatiguetest.service

import ee.ainsus.fatiguetest.common.CrudService
import ee.ainsus.fatiguetest.domain.SentencePair
import ee.ainsus.fatiguetest.domain.SentencePairDTO
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import kotlin.math.min

@Service
@Transactional
class SentencePairService : CrudService<SentencePairDTO, SentencePair>() {

    override fun save(entityDTO: SentencePairDTO): SentencePairDTO {
        entityDTO.levenshteinScore = calculateLevenshteinScore(entityDTO.originalText, entityDTO.fixedText)
        return super.save(entityDTO)
    }

    private fun calculateLevenshteinScore(originalText: String, fixedText: String): Long {
        if(originalText == fixedText) { return 0 }
        if(originalText.isEmpty()) { return fixedText.length.toLong() }
        if(fixedText.isEmpty()) { return originalText.length.toLong() }

        val originalTextLength = originalText.length + 1
        val fixedTextLength = fixedText.length + 1

        var cost = Array(originalTextLength) { it }
        var newCost = Array(originalTextLength) { 0 }

        for (i in 1 until fixedTextLength) {
            newCost[0] = i

            for (j in 1 until originalTextLength) {
                val match = if(originalText[j - 1] == fixedText[i - 1]) 0 else 1

                val costReplace = cost[j - 1] + match
                val costInsert = cost[j] + 1
                val costDelete = newCost[j - 1] + 1

                newCost[j] = min(min(costInsert, costDelete), costReplace)
            }

            val swap = cost
            cost = newCost
            newCost = swap
        }

        return cost[originalTextLength - 1].toLong()
    }
}

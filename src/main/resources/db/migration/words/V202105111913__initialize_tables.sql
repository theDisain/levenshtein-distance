CREATE SCHEMA words;

CREATE TABLE words.sentence_pair
(
    id                bigserial NOT NULL PRIMARY KEY,
    original_text     text      NOT NULL,
    fixed_text        text      NOT NULL,
    levenshtein_score bigint
);
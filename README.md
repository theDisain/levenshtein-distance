# Sentence levenshtein distance calculator

Originated from a weekend project with the attempt to make a CRUD generator. In its essence, this is a boilerplate that is ready to become a full application the moment the correct data model is built.

SWAGGER URL: http://localhost:8082/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

SWAGGER URL(DOCKER): http://localhost:8083/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

TO START:

1) clone this project
2) Run task docker (./gradlew docker)
3) Run task dockerComposeUp (./gradlew dockerComposeUp)
   1) To validate package running or to start without gradle, run "docker-compose up"
4) Navigate to the swagger provided in SWAGGER URL(DOCKER)
5) You should now have a running application with all endpoints available.
import React, {FormEvent} from 'react';
import './App.css';
import {Button, Grid, Input} from "@material-ui/core";
import axios, {AxiosResponse} from "axios";

const CUSTOMER_API_URL = 'http://localhost:8083/api/sentence';
type LevenshteinScore = {
    originalText: string,
    fixedText: string,
    levenshteinScore: number
}

class App extends React.Component<any, LevenshteinScore> {

    constructor(props: any) {
        super(props);

        this.state = {
            originalText: "",
            fixedText: "",
            levenshteinScore: 0
        };
    }

    getLevenshtein = (): void => {
        console.log(CUSTOMER_API_URL)
        axios.post(`${CUSTOMER_API_URL}`, {
            originalText: this.state.originalText,
            fixedText: this.state.fixedText
        }).then((response: AxiosResponse) => {
            this.setState({
                levenshteinScore: response.data.levenshteinScore
            })
        });
    }


    handleEnter = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        this.getLevenshtein();
    };

    render() {
        return (
            <Grid
                container
                spacing={1}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{minHeight: '100vh'}}
            >

                <Grid item zeroMinWidth>
                    <form onSubmit={this.handleEnter}>
                        <Input
                            id="originalText"
                            placeholder="Kirjuta siia"
                            onChange={(s) => this.setState({
                                originalText: s.target.value
                            })}
                            inputMode={"text"}
                            style={{marginRight: '2em', width: '50vh', height: '50vh'}}
                        />
                        <Input
                            id="fixedText"
                            placeholder="Kirjuta siia parandatud variant"
                            onChange={(s) => this.setState({
                                fixedText: s.target.value
                            })}
                            inputMode={"text"}
                            style={{width: '50vh', height: '50vh'}}

                        />

                        <div style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: "center",
                            fontSize: "x-large"
                        }}>{this.state.levenshteinScore}</div>
                        <div style = {{display:'flex', justifyContent:'center', alignItems:'center'}}>
                            <Button onClick={this.getLevenshtein} onSubmit={this.getLevenshtein} style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: "center",
                                fontSize: "x-large"
                            }}>Kliki siia et
                                uuendada</Button>
                        </div>
                    </form>
                </Grid>
            </Grid>
        );
    }
}

export default App;
